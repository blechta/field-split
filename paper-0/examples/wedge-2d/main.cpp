// Copyright (C) 2013 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <petsc.h>
#include <dolfin.h>

#include "../../common/linear_solver.h"
#include "../../common/parameters.h"

#include "../../forms/Compaction2D.h"
#include "../../forms/Compaction2D_PC.h"

#if defined(HAS_PETSC)

// Define boundaries
class Gamma0 : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[0] + x[1] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class Gamma1 : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[1] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class Gamma2 : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
    return ((std::abs(x[1]) < DOLFIN_EPS ||
             std::abs(x[0]-1.0) < DOLFIN_EPS) && on_boundary);
  }
};

class Batchelor_u : public dolfin::Expression
{
public:
  Batchelor_u(double beta) : Expression(2), beta(beta) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double xx = x[0];
    const double yy = x[1] - 1.0;
    const double cc = beta*sin(beta)/(beta*beta - sin(beta)*sin(beta));
    const double cd = (beta*cos(beta) - sin(beta))/(beta*beta
                                                    - sin(beta)*sin(beta));
    const double theta = -atan(yy/xx);
    const double rr = std::sqrt(xx*xx + yy*yy);
    const double ur = cc*theta*sin(theta) + cd*(sin(theta) + theta*cos(theta));
    const double ut = cc*(sin(theta) - theta*cos(theta)) + cd*theta*sin(theta);
    const double costheta = cos(theta);
    const double sintheta = sin(theta);
    const double ux = costheta*ur + sintheta*ut;
    const double uy = -(sintheta*ur - costheta*ut);
    values[0] = ux;
    values[1] = uy;
  }
private:
  const double beta;
};

// Permeability function k(x)
class Permeability : public dolfin::Expression
{
public:
  Permeability(double alpha) : alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double rr = std::sqrt(x[0]*x[0] + x[1]*x[1]);
    values[0] = 0.9 * (1.0 + tanh(-2.0*rr));
  }
private:
  const double alpha;
};

int main(int argc, char* argv[])
{
  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();
  parameters.add("batchelor_flow", false);

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Mesh (see README.md on getting the meshes)
  const std::string mesh_file = parameters["mesh_file"];
  dolfin::Mesh mesh(mesh_file);

  // Model constants

  // (rzeta - 2/3), rzeta = bulk/shear viscosity
  const dolfin::Constant alpha(parameters["alpha"]);

  // Porosity
  const dolfin::Constant phi0(parameters["phi0"]);

  // For Batchelor cornerflow solution
  const double beta = 0.25*DOLFIN_PI;

  // Function spaces
  Compaction2D::FunctionSpace W(mesh);
  dolfin::cout << "Number of dofs: " << W.dim() << dolfin::endl;
  std::shared_ptr<dolfin::SubSpace> W0(new dolfin::SubSpace(W, 0));
  dolfin::SubSpace W1(W, 1);

  // Create functions
  dolfin::Function w(W);

  // Set-up boundary conditions
  dolfin::Constant usl(1.0/sqrt(2.0), -1.0/sqrt(2.0));
  dolfin::Constant zero_vector(0.0, 0.0);
  Gamma0 gamma0;
  Gamma1 gamma1;
  dolfin::DirichletBC bc0(*W0, usl, gamma0);
  dolfin::DirichletBC bc1(*W0, zero_vector, gamma1);

  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc0);
  bcs.push_back(&bc1);

  std::shared_ptr<dolfin::DirichletBC> bc2;
  if ((bool) parameters["batchelor_flow"])
  {
    std::shared_ptr<dolfin::GenericFunction>  u(new Batchelor_u(beta));
    std::shared_ptr<dolfin::SubDomain> gamma2(new Gamma2);
    bc2.reset(new dolfin::DirichletBC(W0, u, gamma2));
    bcs.push_back(bc2.get());
  }

  // Create forms for the "Stokes" part of the McKenzie equations
  Permeability k(alpha);
  dolfin::Constant f(0.0, 1.0);
  dolfin::Constant fp(0.0);

  Compaction2D::BilinearForm a(W, W);
  Compaction2D::LinearForm L(W);
  L.f   = f;
  L.fp  = fp;
  L.phi = phi0;
  L.k   = k;
  a.alpha = alpha;
  a.k   = k;

  // Preconditioner form
  Compaction2D_PC::BilinearForm a_P(W, W);
  dolfin::Constant sgn(1.0);
  if (std::string(parameters["split_type"]) == "schur")
    sgn = -1.0;
  a_P.alpha = alpha;
  a_P.k = k;
  a_P.sgn = sgn;

  // Solve problem
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
        parameters["tol"], w);
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
