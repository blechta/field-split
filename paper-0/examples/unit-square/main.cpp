// Copyright (C) 2013 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <limits>
#include <petsc.h>
#include <dolfin.h>

#include "../../common/create_field_split.h"
#include "../../common/parameters.h"
#include "../../common/linear_solver.h"
#include "../../common/set_nullspace.h"

#include "../../forms/Compaction2D.h"
#include "../../forms/Compaction2D_PC.h"

#if defined(HAS_PETSC)

// Define boundary domain
class Boundary : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return on_boundary; }
};

// Eaxct solution
class Inflow : public dolfin::Expression
{
public:
  Inflow(double c1, double c2, double t1, double t2)
    : Expression(2), c1(c1), c2(c2), t1(t1), t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double kk   = c2*(tanh(t1*x[0] - t2) + tanh(t1*x[1] - t2) + 2.0 + c1);
    const double dpdx = 4*DOLFIN_PI*sin(4*DOLFIN_PI*x[0])*cos(2*DOLFIN_PI*x[1]);
    const double dpdy = 2*DOLFIN_PI*cos(4*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1]);
    const double w1   = sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]) + 2.0;
    const double w2   = 0.5*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdx + w1;
    values[1] = kk*dpdy + w2;
  }
private:
  double c1, c2, t1, t2;
};

class Exact_p : public dolfin::Expression
{
public:
  Exact_p(double c1, double c2) : c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = (-cos(4*DOLFIN_PI*x[0]))*cos(2*DOLFIN_PI*x[1]); }
private:
  double c1, c2;
};

// Permeability
class Permeability : public dolfin::Expression
{
public:
  Permeability(double c1, double c2, double t1, double t2)
    : c1(c1), c2(c2), t1(t1), t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = c2*(tanh(t1*x[0] - t2)+tanh(t1*x[1] - t2) +2.0 +c1); }
private:
  double c1, c2, t1, t2;
};

// Source term
class Source : public dolfin::Expression
{
public:
  Source(double c1, double c2, double t1, double t2, double alpha)
    : Expression(2), c1(c1), c2(c2), t1(t1), t2(t2), alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double pi3 = DOLFIN_PI*DOLFIN_PI*DOLFIN_PI;
    const double pi2 = DOLFIN_PI*DOLFIN_PI;
    values[0] = -0.5*(-16.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-8.0*DOLFIN_PI*c2*t1*t1*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))*tanh(t1*x[1]-t2)-16.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-4.0*pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]))+(-alpha-0.5)*(-16.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-8.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))+1.0*pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])+4.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*cos(2.0*DOLFIN_PI*x[1]))+(-alpha-1.0)*(-64.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])-8.0*DOLFIN_PI*c2*t1*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*tanh(t1*x[0]-t2)*cos(2.0*DOLFIN_PI*x[1])+32.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*cos(2.0*DOLFIN_PI*x[1]))+4.0*DOLFIN_PI*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]);

    values[1] = (-alpha-1.0)*(-8.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-4.0*DOLFIN_PI*c2*t1*t1*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))*tanh(t1*x[1]-t2)+8.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-2.0*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))+(-alpha-0.5)*(-32.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)+16.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-8.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*sin(2.0*DOLFIN_PI*x[1])+2.0*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))-0.5*(-32.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-4.0*DOLFIN_PI*c2*t1*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*tanh(t1*x[0]-t2)*sin(2.0*DOLFIN_PI*x[1])-16.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*sin(2.0*DOLFIN_PI*x[1])-0.5*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))+2.0*DOLFIN_PI*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]);

  }
private:
  double c1, c2, t1, t2, alpha;
};


int main(int argc, char* argv[])
{
  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();
  parameters.add("n", 32, 1, std::numeric_limits<int>::max());

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Organise model parameters
  const double k_low = parameters["k_min"];
  const double k_up  = parameters["k_max"];
  const dolfin::Constant alpha(parameters["alpha"]);

  const double t1 = 10.0;
  const double t2 = 5.0;
  const double frakx = 2.0*tanh(t1 - t2) + 2.0;
  const double fraky = 2.0*tanh(-t2) + 2.0;
  const double c1 = (k_up*fraky - k_low*frakx)/(k_low-k_up);
  const double c2 = (k_up-k_low)/(frakx - fraky);

  // Mesh
  const std::size_t nx = parameters["n"];
  dolfin::UnitSquareMesh mesh(nx, nx);

  // Function spaces
  Compaction2D::FunctionSpace W(mesh);
  dolfin::cout << "Number of degrees of freedom: " << W.dim() << dolfin::endl;
  dolfin::SubSpace W0(W, 0);

  // Set-up boundary conditions
  Boundary boundary;
  Inflow velocity_bc(c1, c2, t1, t2);
  dolfin::DirichletBC bc(W0, velocity_bc, boundary);
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc);

  // Coefficient functions
  dolfin::Constant phi(0.0);
  Source f(c1, c2, t1, t2, alpha);
  Permeability k(c1, c2, t1, t2);
  dolfin::Constant fp(0.0);

  // Create forms
  Compaction2D::BilinearForm a(W, W);
  Compaction2D::LinearForm L(W);
  L.f     = f;
  L.fp    = fp;
  L.k     = k;
  L.phi   = phi;
  a.alpha = alpha;
  a.k     = k;

  // Preconditioner form
  Compaction2D_PC::BilinearForm a_P(W, W);
  dolfin::Constant sgn(1.0);
  if (std::string(parameters["split_type"]) == "schur")
    sgn = -1.0;
  a_P.sgn = sgn;
  a_P.alpha = alpha;
  a_P.k = k;

  // Solve problem
  dolfin::Function w(W);
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
        parameters["tol"], w);

}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
