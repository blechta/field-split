# Sander Rhebergen
# University of Oxford
# Last update: 12 April 2013
#
# Solving Stokes with compaction.
#
#**********************************************************************************************


from dolfin import *
import numpy
from math import tanh

def compute(nx,klow,degree):

    if (abs(klow-1.0)<1.e-12):
        if (degree==21):
            if (nx==16):
                errors = {'u - u_e': 0.148723,
                          'v - v_e': 0.131818,
                          'p - p_e': 0.0483695}
            elif (nx==32):
                errors = {'u - u_e': 0.0114326,
                          'v - v_e': 0.010037,
                          'p - p_e': 0.0126243}
            elif (nx==64):
                errors = {'u - u_e': 0.000758076,
                          'v - v_e': 0.000687087,
                          'p - p_e': 0.00319109}
            elif (nx==128):
                errors = {'u - u_e': 4.56033E-05,
                          'v - v_e': 4.67051E-05,
                          'p - p_e': 0.000800024}
            elif (nx==256):
                errors = {'u - u_e': 4.32619E-06,
                          'v - v_e': 4.58417E-06,
                          'p - p_e': 0.00020013}

    if (abs(klow-(0.0))<1.e-12):
        if (degree==21):
            if (nx==16):
                errors = {'u - u_e': 0.1486,
                          'v - v_e': 0.131637,
                          'p - p_e': 0.0512653}            
            elif (nx==32):
                errors = {'u - u_e': 0.0115216,
                          'v - v_e': 0.0100088,
                          'p - p_e': 0.013584}
            elif (nx==64):
                errors = {'u - u_e': 0.00076862,
                          'v - v_e': 0.00068685,
                          'p - p_e': 0.00345555}
            elif (nx==128):
                errors = {'u - u_e': 4.67477E-05,
                          'v - v_e': 4.66362E-05,
                          'p - p_e': 0.00086812}
            elif (nx==256):
                errors = {'u - u_e': 4.11438E-06,
                          'v - v_e': 4.22421E-06,
                          'p - p_e': 0.000217429}


    #kup = 1.0E-4
    if (abs(klow-(11.0))<1.e-12):
        if (degree==21):
            if (nx==16):
                errors = {'u - u_e': 0.000167147,
                          'v - v_e': 0.000173163,
                          'p - p_e': 0.0178969}
            elif (nx==32):
                errors = {'u - u_e': 1.34534E-05,
                          'v - v_e': 1.31805E-05,
                          'p - p_e': 0.00411055}
            elif (nx==64):
                errors = {'u - u_e': 2.13018E-06,
                          'v - v_e': 1.64044E-06,
                          'p - p_e': 0.00100587}
            elif (nx==128):
                errors = {'u - u_e': 5.23578E-07,
                          'v - v_e': 3.68973E-07,
                          'p - p_e': 0.000250413}            
            elif (nx==256):
                errors = {'u - u_e': 1.52433E-07,
                          'v - v_e': 1.1798E-07,
                          'p - p_e': 6.3718E-05}

    return errors

# Run for different meshes
degree=21
klow=11.0

h = []  # element sizes
E = []  # errors
for nx in [16, 32, 64, 128, 256]:
    h.append(1.0/nx)
    E.append(compute(nx, klow, degree))  # list of dicts

# Convergence rates
from math import log as ln  # log is a dolfin name too
error_types = E[0].keys()
for error_type in sorted(error_types):
    print '\nError norm based on', error_type
    for i in range(1, len(E)):
        Ei   = E[i][error_type]  # E is a list of dicts
        Eim1 = E[i-1][error_type]
        r = ln(Ei/Eim1)/ln(h[i]/h[i-1])
        print 'h=%8.2E E=%8.2E r=%.2f' % (h[i], Ei, r)
    for i in range(1, len(E)):
        Ei   = E[i][error_type]  # E is a list of dicts
        Eim1 = E[i-1][error_type]
        r = ln(Ei/Eim1)/ln(h[i]/h[i-1])
        print '%8.2E & %.1f &' % (Ei, r)

