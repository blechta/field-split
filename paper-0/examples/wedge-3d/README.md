# 3D wedge problem

## Downloading meshes

Mesh file are on Bitbucket. To get the mesh XML files, from this
directory:

    1. wget https://bitbucket.org/magma-dynamics/preconditioning/downloads/wedge3D-meshes.tar.xz
    2. tar xf wedge3D-meshes.tar.xz


## Reproducing results in Table 6.5

Run the script

    python run-tests.py